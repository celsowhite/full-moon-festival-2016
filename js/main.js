$(document).ready(function(){

  /*======================== 
  DEVICE DETECTION
  ========================*/

  var isMobile = false; //initiate as false

if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
  
  /*======================== 
  NAVIGATION
  ========================*/

  /*=== Smooth Scroll ===*/

  // Desktop nav

  $('.main_navigation li a').smoothScroll({
    offset: 30
  });

  // Adjust offset for mobile to account for the fixed header

  $('.mobile_navigation_list li a').smoothScroll({
    offset: -60
  });

  /*=== Add Navigation Indicators ===*/

  $(window).scroll(function(){

    var lineupPosition = $('#artists_list_container').offset().top;
    var locationPosition = $('#governors_island_container').offset().top - 80;
    var storyPosition = $('#story_container').offset().top - 50;
    var partnersPosition = $('#partners_container').offset().top - 50;
    var scroll = $(this).scrollTop();

    if(lineupPosition <= scroll && scroll <= locationPosition) {
      $('.main_navigation li a').removeClass('active');
      $('#lineup_link').addClass('active');
    }
    else if (locationPosition <= scroll && scroll <= storyPosition) {
      $('.main_navigation li a').removeClass('active');
      $('#location_link').addClass('active');
    }
    else if (storyPosition <= scroll && scroll <= partnersPosition) {
      $('.main_navigation li a').removeClass('active');
      $('#story_link').addClass('active');
    }
    else if (partnersPosition <= scroll) {
      $('.main_navigation li a').removeClass('active');
      $('#partners_link').addClass('active');
    }
    else {
      $('.main_navigation li a').removeClass('active');
    }


  });

  /*======================== 
  MOBILE NAVIGATION
  ========================*/

  /*=== Open the fullscreen overlay ===*/

  $('.menu_icon').click(function(){
    $('.mobile_navigation').fadeIn('fast');
  });

  /*=== Clicking the x within the overlay closes it ===*/

  $('.close_menu_icon').click(function(){
    $('.mobile_navigation').fadeOut('fast');
  });

  /*=== Clicking a navigation link within the overlay scrolls to the section of the page and closes it ===*/

  $('.mobile_navigation_list li a').click(function(){
    $('.mobile_navigation').fadeOut('fast');
  });

  /*======================== 
  FADEIN GET TICKETS BUTTON
  ========================*/

  $(window).scroll(function(){

    /*=== Define the position of the static button and the position of the scroll ===*/

    var staticTickets = $('.get_tickets .primary_button').offset().top;
    var scroll = $(window).scrollTop();

    /*=== Display/Hide the fixed button depending on where the user has scrolled in the page ===*/

    if(scroll > staticTickets) {
      $('.get_tickets_fixed').addClass('active');
    }
    else {
      $('.get_tickets_fixed').removeClass('active');
    }

  });

  /*======================== 
  ARTIST PASS HOVER
  ========================*/

  $('.saturday_pass_link').hover(function(){
  	$('.saturday_pass').toggleClass('active');
    $('.sunday_pass').toggleClass('nonactive');
    $('.no_pass').toggleClass('nonactive');	
  });

  $('.sunday_pass_link').hover(function(){
    $('.sunday_pass').toggleClass('active');
  	$('.saturday_pass').toggleClass('nonactive');
    $('.no_pass').toggleClass('nonactive');	
  });

  /*======================== 
  ARTIST NAME HOVER SHOW ILLUSTRATION IN PLACE OF LOGO
  ========================*/
  
  /*=== FadeOut the moon, FadeIn the artist illustration ===*/

  var handlerIn = function() {

    var artistName = $(this).attr('data-artist');
    
    if(artistName !== undefined) {

      // Fade out all artists to prevent artist getting stuck glitch

      $('.logo_date_illustrations .artist').fadeOut(1);

      // Fade out moon, fade in artist

      $('.logo_date').fadeOut(1, function(){

         $('.logo_date_illustrations .' + artistName).fadeIn(200);

      });
    }

  };

  /*=== FadeOut the artist, FadeIn the moon ===*/

  var handlerOut = function() {

    var artistName = $(this).attr('data-artist');

    if(artistName !== undefined) {

      $('.logo_date_illustrations .artist').fadeOut(1, function(){

         $('.logo_date').fadeIn(1);

      });
    }
    
  };

  if(!isMobile) {
    $('.top_artists_list li').hover(handlerIn,handlerOut);
  } 

  /*======================== 
  CUSTOM VIMEO PLAYER
  ========================*/

  var iframe = $('#fullmoonvideo')[0];
  var player = $f(iframe);

  /*=== Setup play and pause functions ===*/

  var onPlay = function() {
    $('.festival_video_container img').fadeOut("slow", function(){
      $('.pause').removeClass('active');
      $('.play').addClass('active');
    });
  };

  var onPause = function() {  
      $('.play').removeClass('active');
      $('.pause').addClass('active');
  };

  /*=== When player is ready add event listeners for play and pause ===*/

  player.addEvent('ready', function() {
    player.addEvent('play', onPlay);
    player.addEvent('pause', onPause);
  });

  /*=== Click functions for custom html buttons ===*/

  $('.play').click(function(){
  		player.api('play');
  });

  $('.pause').click(function(){
  		player.api('pause');
  });

  /*======================== 
  FITVIDS
  ========================*/

  $(".video_container").fitVids();

});